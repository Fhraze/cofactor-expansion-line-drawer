import matplotlib.pyplot as plt
import os
import sys
import csv

def render(coords, matrix):
    x_values = [coord[0] for coord in coords]
    y_values = [coord[1] for coord in coords]

    for y in range(0, sizey):
        for x in range(0, sizex):
            plt.annotate(matrix[y+((curr_matrix-1)*sizey)][x], (x+0.5, y+0.5), ha='center', va='center', fontname='monospace', fontsize='42')

    plt.plot(x_values, y_values, linewidth=10)
    plt.gca().invert_yaxis()
    plt.xticks(sizex_arr)
    plt.yticks(sizey_arr)
    plt.gca().set_xticklabels([])
    plt.gca().set_yticklabels([])
    plt.grid(True)
    plt.suptitle(f"Matrix {curr_matrix}", fontsize=24, fontname='monospace', fontweight='bold')
    plt.savefig(f"matrices/matrix{curr_matrix}")
    plt.clf()

def get_cords(matrix):
    coords = []
    for y in range(0, sizey):
        for x in range(0, sizex):
            if matrix[y+((curr_matrix-1)*sizey)][x] == '1':
                coord = (x+0.5, y+0.5)
                coords.append(coord)
    return coords

arg_len = len(sys.argv)
if arg_len == 1 or arg_len > 2:
    print("ERROR! matrices csv file required.")
    exit()
elif arg_len > 2:
    print("ERROR! too many arguments.")
    exit()

csv_path = str(sys.argv[1])

sizex = int(input("(?x?) horizontal size of the matrices: "))
sizey = int(input(f"({sizex}x?) vertical size of the matrices: "))
nOf_matrices = int(input(f"({sizex}x{sizey}) number of matrices: "))
sizex_arr = range(0, sizex+1)
sizey_arr = range(0, sizey+1)
curr_matrix = 1

csv_matrix = list(csv.reader(open(csv_path)))

os.system("mkdir matrices")
for i in range(1, nOf_matrices+1):
    print(f"Processing matrix {curr_matrix}...")
    render(get_cords(csv_matrix), csv_matrix)
    curr_matrix += 1
    print("Ok!")

print("\nDone processing all the matrices! their images should be located inside a \"matrices\" folder.")

