use std::process::exit;
use std::io::{self, Write};
use rand::{self, Rng};
use std::cmp::Ordering;
use k_board::{keyboard::Keyboard, keys::Keys};

// Common values
const MIN_ROOMSCALE: u16 = 7;
const MAX_ROOMSCALE: u16 = 15;

// Sprites
const WALL: char = '█';
const FLOOR: char = '░';
const PLAYER_DOWN: char = '╦';
const PLAYER_UP: char = '╩';
const PLAYER_RIGHT: char = '╠';
const PLAYER_LEFT: char = '╣';

struct Room {
    row: u16,
    col: u16,
    height: u16,
    width: u16,
}

struct Player {
    row: u16,
    col: u16,
    spr: char,
}

#[tokio::main]
async fn main() {
    let size = termsize::get().unwrap();
    println!("Terminal size: {}x{}", size.cols, size.rows);
    
    if size.cols < 30 || size.rows < 31 {
        println!("Error: terminal size too small! (min: 30x31)");
        exit(0);
    }
    
    // Room generation
    let rooms = gen_structures();
    term_clear();
    // Render rooms
    let mut rooms_buffer: [[char; 30]; 30] = [[' '; 30]; 30];
    for i in rooms.iter() {
        render_room(i);
        render_room_to_arr(i, &mut rooms_buffer);
    }

    // Player
    let spawn_room = rand::thread_rng().gen_range(0, 4);
    let mut player = {
        Player {
            row: rand::thread_rng().gen_range(
                    rooms[spawn_room].row + 1,
                    rooms[spawn_room].row + (rooms[spawn_room].height - 2)
                ),
            col: rand::thread_rng().gen_range(
                    rooms[spawn_room].col + 1,
                    rooms[spawn_room].col + (rooms[spawn_room].width - 2 )
                ),
            spr: PLAYER_DOWN,
        }
    };
    render_player(&player);

    handle_input(&mut player, &rooms_buffer).await;
    loop {
        
    }
}

// Game stuff
fn gen_structures() -> [Room; 4] {
    let r1_h: u16 = rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE);
    let r1_w: u16 = rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE);
    let room_1 = Room {
        row: 1,
        col: 1,
        height: r1_h,
        width: r1_w,
    };
    let r2_h: u16 = rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE);
    let r2_w: u16 = rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE);
    let room_2 = Room {
        row: 1,
        col: r1_w + 3,
        height: r2_h,
        width: r2_w,
    };
    let r3_w = rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE);
    let room_3 = Room {
        row: r1_h + 1,
        col: 1,
        height: rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE),
        width: r3_w,
    };
    let room_4 = Room {
        row: match r1_h.cmp(&r2_h) {
            Ordering::Less => r2_h + 1,
            Ordering::Greater => r1_h + 1,
            Ordering::Equal => r1_h + 1,
        },
        col: r3_w + 2,
        height: rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE),
        width: rand::thread_rng().gen_range(MIN_ROOMSCALE, MAX_ROOMSCALE),
    };

    [room_1, room_2, room_3, room_4]
}

fn render_room(room: &Room) {
    cursor_move(room.row, room.col); // Move cursor to room's position
    for _ in 1..room.width { // Draw the top of the room
        print!("{}", WALL);
    }
    for i in (room.row + 1)..(room.row + (room.height - 2)) { // Iterate through the middle of the room
        cursor_move(i, room.col);
        print!("{}", WALL);
        for _ in 1..(room.width - 2) {
            print!("{}", FLOOR);
        }
        print!("{}", WALL);
    }
    cursor_move(room.row + (room.height - 2), room.col); // Move cursor to the bottom of the room
    for _ in 1..room.width { // Draw the bottom of the room
        print!("{}", WALL);
    }
    cursor_move(31, 1);
    let _ = io::stdout().flush();
}

fn render_room_to_arr(room: &Room, array: &mut [[char; 30]; 30]) {
    for i in room.col..(room.col + (room.width - 1)) {
        array[ usize::try_from(room.row).unwrap() - 1 ][ usize::try_from(i).unwrap() - 1 ] = WALL;    
    }
    for i in 1..(room.height - 2) {
        array[ usize::try_from(room.row + i).unwrap() - 1 ][ usize::try_from(room.col).unwrap() - 1 ] = WALL;    
        for j in (room.col)..(room.col + (room.width - 3)) {
            array[ usize::try_from(room.row + i).unwrap() - 1 ][ usize::try_from(j).unwrap() ] = FLOOR;    
        }
        array[ usize::try_from(room.row + i).unwrap() - 1 ][ usize::try_from(room.col + (room.width - 2)).unwrap() - 1 ] = WALL;    
    }
    for i in room.col..(room.col + (room.width - 1)) {
        array[ usize::try_from(room.row + (room.height - 2)).unwrap() - 1 ][ usize::try_from(i).unwrap() - 1 ] = WALL;    
    }
}

fn replace_from_arr(row: u16, col: u16, rooms_buffer: &[[char; 30]; 30]) {
    cursor_move(row, col);
    print!("{}", rooms_buffer[ usize::try_from(row-1).unwrap() ][ usize::try_from(col-1).unwrap() ]);
    cursor_move(31, 1);
    let _ = io::stdout().flush();
}

// Player stuff
fn render_player(player: &Player) {
    cursor_move(player.row, player.col);
    print!("{}", player.spr);
    cursor_move(31, 1);
    let _ = io::stdout().flush();
}

fn player_walk(player: &mut Player, direction: char, rooms_buffer: &[[char; 30]; 30]) {
    match direction {
        'w' => {
            // POSSIBLE BUG HERE !!
            player.spr = PLAYER_UP;
            if rooms_buffer[ usize::try_from(player.row - 2).unwrap() ]
                            [ usize::try_from(player.col - 1).unwrap() ] == WALL
                || rooms_buffer[ usize::try_from(player.row - 2).unwrap() ]
                            [ usize::try_from(player.col - 1).unwrap() ] == ' '
                || player.row == 1 {
                render_player(&player);
            } else {
                replace_from_arr(player.row, player.col, &rooms_buffer);
                player.row = player.row - 1;
                render_player(&player);
            }
        },
        's' => {
            player.spr = PLAYER_DOWN;
            if rooms_buffer[ usize::try_from(player.row).unwrap() ]
                            [ usize::try_from(player.col - 1).unwrap() ] == WALL
                || rooms_buffer[ usize::try_from(player.row).unwrap() ]
                            [ usize::try_from(player.col - 1).unwrap() ] == ' '
                || player.row == 30 {
                render_player(&player);
            } else {
                replace_from_arr(player.row, player.col, &rooms_buffer);
                player.row = player.row + 1;
                render_player(&player);
            }
        },
        'd' => {
            player.spr = PLAYER_RIGHT;
            if rooms_buffer[ usize::try_from(player.row - 1).unwrap() ]
                            [ usize::try_from(player.col).unwrap() ] == WALL
                || rooms_buffer[ usize::try_from(player.row - 1).unwrap() ]
                            [ usize::try_from(player.col).unwrap() ] == ' '
                || player.col == 30 {
                render_player(&player);
            } else {
                replace_from_arr(player.row, player.col, &rooms_buffer);
                player.col = player.col + 1;
                render_player(&player);
            }
        },
        'a' => {
            player.spr = PLAYER_LEFT;
            if rooms_buffer[ usize::try_from(player.row - 1).unwrap() ]
                            [ usize::try_from(player.col - 2).unwrap() ] == WALL
                || rooms_buffer[ usize::try_from(player.row - 1).unwrap() ]
                            [ usize::try_from(player.col - 2).unwrap() ] == ' '
                || player.col == 1 {
                render_player(&player);
            } else {
                replace_from_arr(player.row, player.col, &rooms_buffer);
                player.col = player.col - 1;
                render_player(&player);
            }
        },
        _ => (),
    }
}

// Terminal stuff
async fn handle_input(mut player: &mut Player, rooms_buffer: &[[char; 30]; 30]) {
    cursor_move(31, 1);
    for key in Keyboard::new() {
        match key {
            Keys::Up => {
                player_walk(&mut player, 'w', rooms_buffer);
                cursor_move(31, 1);
                print!("      ");
                cursor_move(31, 1);
            }
            Keys::Down => {
                player_walk(&mut player, 's', rooms_buffer);
                cursor_move(31, 1);
                print!("      ");
                cursor_move(31, 1);
            }
            Keys::Right => {
                player_walk(&mut player, 'd', rooms_buffer);
                cursor_move(31, 1);
                print!("      ");
                cursor_move(31, 1);
            }
            Keys::Left => {
                player_walk(&mut player, 'a', rooms_buffer);
                cursor_move(31, 1);
                print!("      ");
                cursor_move(31, 1);
            }
            Keys::Ctrl('q') => {
                exit(0);
            }
            _ => {
                ();
            }
        }
    }
}

fn term_clear() {
    print!("\x1b[2J");
    let _ = io::stdout().flush();
}

fn cursor_move(row: u16, col: u16) {
    print!("\x1b[{};{}H", row, col);
    let _ = io::stdout().flush();
}
